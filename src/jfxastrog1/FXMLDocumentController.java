/*
 */
package jfxastrog1;

import java.net.URL;
import java.time.Instant;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;

/**
 *
 * @author maria
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label value_1;
    @FXML
    private Label value_2;
    @FXML
    private Label value_3;
    @FXML
    private Label value_4;
    @FXML
    private Label value_5;
    @FXML
    private Label value_6;

//    Font myFontloadFontAirstreamNF20
//            = Font.loadFont(getClass()
//                    .getResourceAsStream("/res/digit.ttf"), 48);
    //Timer class
    Timer timer = new Timer();
    TimerTask repeatedTask = new TimerTask() {
    //when start the puzzle
        Date dateStart = new Date();
        @Override
        public void run() {
            long howMuchSecs = ( new Date().getTime() - dateStart.getTime()) / 1000;
            System.out.println("Secondi passati :" + howMuchSecs);
        }
    };
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Date start
        
        Instant istante  = new Date().toInstant() ;    //Start at time...
                
        //Timer scheduler
        final long SECS = 1000L * 2;
        timer.scheduleAtFixedRate(repeatedTask, 0, SECS);
    }

    @FXML
    private void handleWheelScroll(ScrollEvent event) {
        if (!(event.getSource() instanceof GridPane)) {
            return;
        }
        GridPane w = (GridPane) event.getSource();
        ObservableList ol = w.getChildren();
        int value = Integer.parseInt(((Label) (ol.get(1))).getText().trim());
        if (event.getDeltaY() < 0) {
            if (++value >= 9) {
                value = 0;
            }
        } else {
            if (--value <= 0) {
                value = 9;
            }
        }
        ((Label) ol.get(0)).setText((value - 1 < 0) ? (9 + " ") : (value - 1 + " "));
        ((Label) ol.get(1)).setText(value + " ");
        ((Label) ol.get(2)).setText((value + 1 > 9) ? (0 + " ") : (value + 1 + " "));

        checkPuzzleEnd();
//        w.requestLayout();
    }

    private void checkPuzzleEnd() {
        //Utilizzando gli id:fx preleva dalle Label i valori utili al match.
        //Solo i Label centrali (quelli posizionati sulla freccia per intenderci) 
        //sono dotati di id:fx(value_1..value_5)

        //final String contenente numero x il match
        final String sMatch = "123123";
        String sValue
                = value_1.getText().trim()
                + value_2.getText().trim()
                + value_3.getText().trim()
                + value_4.getText().trim()
                + value_5.getText().trim()
                + value_6.getText().trim();
        //Se matcha ritorna 100(considerare la possibilità di uscire con valore 
        //percentuale di gioco.
        if (sValue.compareTo(sMatch) == 0) {
            System.out.println(
                    String.format("Puzzle risolto in ore:%d, minuti:%d, secondi:%d",
                             0, 0, 0));
            System.exit(100);
        }
    }

}
